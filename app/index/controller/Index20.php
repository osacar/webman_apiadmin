<?php


namespace app\index\controller;


use app\model\MysqlField;
use app\service\encryption\AES;
use app\service\encryption\Opensource;
use app\service\encryption\Rsa;
use app\service\statistics\Clients\StatisticClient;
use think\facade\Db;

class Index20
{

    public function index()
    {
        $list = Db::table('kc_sources')->limit(10)->select()->toArray();
        foreach ($list as $key=>$value){
            if(empty($value['x_status'])){
                $list[$key]['x'] = "+".$value['x'];
            }else{
                $list[$key]['x'] = "-".$value['x'];
            }
            if(empty($value['y_status'])){
                $list[$key]['y'] = "+".$value['y'];
            }else{
                $list[$key]['y'] = "-".$value['y'];
            }
        }
        return json(['code' => 200, 'msg' => 'ddd', 'data' => $list]);
        $data = [
            ['name' => 'jizhan', 'type' => 'effectScatter', 'symbolSize' => 20, 'data' => [[0, 0], [0, 1220], [350, 1171]]],
            ['name' => 'bsk4', 'type' => 'scatter', 'data' => [[161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6], [176.5, 71.8], [164.4, 55.5], [160.7, 48.6], [174.0, 66.4], [163.8, 67.3]]],
            ['name' => 'bsk5', 'type' => 'scatter', 'data' => [[100.2, 511.6], [267.5, 59.0], [259.5, 49.2], [257.0, 63.0], [255.8, 53.6], [276.5, 71.8], [264.4, 55.5], [260.7, 48.6], [274.0, 66.4], [263.8, 67.3]]],
            ['name' => 'bsk6', 'type' => 'scatter', 'data' => [[361.2, 51.6], [367.5, 59.0], [359.5, 49.2], [357.0, 63.0], [355.8, 53.6], [376.5, 71.8], [364.4, 55.5], [360.7, 48.6], [374.0, 66.4], [363.8, 67.3]]],
        ];

        return view('index/index', ['data' => json_encode($data)]);
    }

    public function demo()
    {
        $data = json_encode(['dmobile' => '12345678901']);
        $dd = AES::ecb_encrypt($data, 'd1565f0ad635695b0918f1f9c2004dcf');
        return json(['code' => 200, 'msg' => 'ddd', 'data' => $dd]);
    }

    public function demo1()
    {
        $data = new MysqlField();
        $id = $data->get_table_number(0);
        $demo = $data::suffix($id)->select()->toArray();
        $total = $data->totalNumberPages($id, '');

        //$list = $demo->find();
        return json(['code' => 200, 'msg' => 'ddd', 'data' => [$demo, 'total' => $total]]);
    }

    public function demo2()
    {
        StatisticClient::tick("index", 'index');
        $success = rand(0, 1);
        $code = rand(300, 400);
        $msg = '这个是测试消息';
        var_export(StatisticClient::report('TestModule', 'TestInterface', $success, $code, $msg));
    }


    public function jiami()
    {
        $publicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAreKvmhN3Me3XY1/qztj3HR5T3oUotfG22loFxePSq0n4qxiiuv8ii4Zmy2bkz5pn8e50cWZdQCXs31z4iCYsUc7Xd5kaiy5J+WuW62aNxqdD07IZN/Yw9/oAsaf1ij5ZSsFvZDi4G+JCNM3Lc7spiCJmpDK3wfrEDUqpRNoX0ih9P/uQ0k8KeEkvlLyfxceVXZ/Q6u7lxMVLsQRf3IE2oghPTmCEf0jOaBmke6NaHP5f5OEtQ16zuNXNN8tRyM1rP9KsNHOE3xokbX23gBgXeFGBx+Y44GWmB5Os0zOSVu+LdaUOXloChpvS/43T8Rocr85bSY670FDhbLCAs1wadwIDAQAB';
        $privateKey = 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCt4q+aE3cx7ddjX+rO2PcdHlPehSi18bbaWgXF49KrSfirGKK6/yKLhmbLZuTPmmfx7nRxZl1AJezfXPiIJixRztd3mRqLLkn5a5brZo3Gp0PTshk39jD3+gCxp/WKPllKwW9kOLgb4kI0zctzuymIImakMrfB+sQNSqlE2hfSKH0/+5DSTwp4SS+UvJ/Fx5Vdn9Dq7uXExUuxBF/cgTaiCE9OYIR/SM5oGaR7o1oc/l/k4S1DXrO41c03y1HIzWs/0qw0c4TfGiRtfbeAGBd4UYHH5jjgZaYHk6zTM5JW74t1pQ5eWgKGm9L/jdPxGhyvzltJjrvQUOFssICzXBp3AgMBAAECggEBAKGZLCz6ril+qk82UtfNF0q+Zp9EDSOZ8Z9XTkosghqh8jHAAWvbtFUKoPmjND4jdzqFqz3ALiVxmYOzuV1+bG3S1rfVV6/FYwYjkOxQMK5PwF87AujZKYUrl/2C5FSHwSASSN1RQPjY+0JrhwqVA/H6Vn1+jPv/ergHjOvp631ojWgzS6CerB1ixLMDA4OEGCnuMdvPhEDpgg3XVWlvtEx3Jr/oka6uQLSdbhUELabZmZGO81KoCTuklfqZq365hIBEzH1CYYNUtLlgPhrJt2Nc4uvzxb1Y+uF3RRUHrVAnhadGopXIxkYbbnhUcoix96w0Nepu4LGluMy/WcxGGtECgYEA6VXunJxNrml2rLX1fP/i49tUWiD9U59S/jQNO1USjuFSDtE4k8ouPcjJVVlxwaiO+k+uaNX/CCfHO8AiyV1aNc7fjmFlvTv0TcHIh7kzjQnNnknDxzM0UehrCI9Dwl+ZmDAdlY+9+OimtTUpYk4La+2ab232Isnn3zp3bmK0szUCgYEAvsZ67VoIq4pGYnbo3lWV5KdDvUsTsTKcpmnrSM7MQNL7OKZRDJyGE7DzI1nm3kYnFU4lL/A0hUbfCBoWlKQWjF0/fmwOFEeVnBpfuJHkIvHFgf3mhCHcrwKCVeqfv5dZFjA8DOAl4IOyd9DRw/S7WQ/2ZPdPAwcJ9sdAO0OiAHsCgYEAtZ8RsA1p2cx1d1Ha14VF0h4ytkTfamulagCSHBDH9TclbCsmYsMI40aakDii9x6AzA2hsDCB5pMv3S5c5Wgog6GXGXhufLFiI/QmZp6dpbZvujmojjEpFGhYh70b/Pv0ziUFohjUwh41RO8wWzhsY4H6p6lr4QBP383ekKl8BdECgYA77kY3wgzvNqUd0ZUeI2zPQ+x3ztoE1I15idSsLqrjwmPBTB4yBAnqra/z7w4jzY4Bl2b/REp7g+QUu31UfKfIuJJ1uKRDoqlRPMfNR6ePgrw1NfnwkGKvZ+5h5y8AFR+bZNO4h8HeYgDyutrq4Drqo6UX2skrfk6XA8kd1QVTeQKBgBFFnAwhc3y+C/DskOgrumx+9QPWgujpQyNL7BRCXvhOAbB+sPRBEwpxnsv6rs7BKhZnVnE9RZzeqXP0wSTk5228gd55CkApSE3SRuJji0f0YTrytKk9zFyZbBC/JHnHPALPtMuXLEEC7vyprUlE37pP//2hjqcuu2ZKZLdUTs3+';

        $rsa = new Rsa($publicKey, $privateKey);
        $data = $rsa->encodeByPrivateKey(json_encode(['data' => ['code' => 200, 'msg' => '操你妈']]));
        return json(['code' => 200, 'msg' => 'ddd', 'data' => $data]);
    }

    public function ff()
    {
        ob_implicit_flush(1);
        //需要加密的目录
        $dir = runtime_path().'/logs/';
        if(!is_dir(runtime_path() . '/encoded/')){
            //加密后的文件目录
            mkdir(runtime_path() . '/encoded/');
        }

        $files = glob($dir . '*.log');
        chdir($dir);
        foreach ($files as $file) {
            $target_file = $file;
            $target_file = str_replace('/logs/', '/encoded/', $target_file);
            $options = array(
                //混淆方法名 1=字母混淆 2=乱码混淆
                'ob_function' => 2,
                //混淆函数产生变量最大长度
                'ob_function_length' => 3,
                //混淆函数调用 1=混淆 0=不混淆 或者 array('eval', 'strpos') 为混淆指定方法
                'ob_call' => 1,
                //随机插入乱码
                'insert_mess' => 0,
                //混淆函数调用变量产生模式  1=字母混淆 2=乱码混淆
                'encode_call' => 2,
                //混淆class
                'ob_class' => 0,
                //混淆变量 方法参数  1=字母混淆 2=乱码混淆
                'encode_var' => 2,
                //混淆变量最大长度
                'encode_var_length' => 5,
                //混淆字符串常量  1=字母混淆 2=乱码混淆
                'encode_str' => 2,
                //混淆字符串常量变量最大长度
                'encode_str_length' => 3,
                // 混淆html 1=混淆 0=不混淆
                'encode_html' => 2,
                // 混淆数字 1=混淆为0x00a 0=不混淆
                'encode_number' => 1,
                // 混淆的字符串 以 gzencode 形式压缩 1=压缩 0=不压缩
                'encode_gz' => 1,
                // 加换行（增加可阅读性）
                'new_line' => 0,
                // 移除注释 1=移除 0=保留
                'remove_comment' => 1,
                // debug
                'debug' => 1,
                // 重复加密次数，加密次数越多反编译可能性越小，但性能会成倍降低
                'deep' => 1,
                // PHP 版本
                'php' => 7,
            );
            $op = new Opensource();
            $op->enphp_file($file, $target_file, $options);
        }
        return json(['code' => 200, 'msg' => '加密成功', 'data' => []]);
    }

    public function qq()
    {
        //登录
        $qqOAuth = new \Yurun\OAuthLogin\QQ\OAuth2('appid', 'appkey', 'callbackUrl');
        $url = $qqOAuth->getAuthUrl();
        header('location:' . $url);
        //回调处理
        // 获取accessToken
        $accessToken = $qqOAuth->getAccessToken($qqOAuth->state);
        // 调用过getAccessToken方法后也可这么获取
        // $accessToken = $qqOAuth->accessToken;
        // 这是getAccessToken的api请求返回结果
        // $result = $qqOAuth->result;
        // 用户资料
         $userInfo = $qqOAuth->getUserInfo();
        // 这是getAccessToken的api请求返回结果
        // $result = $qqOAuth->result;
        // 用户唯一标识
        $openid = $qqOAuth->openid;

        //解决第三方登录只能设置一个回调域名的问题
        // 解决只能设置一个回调域名的问题，下面地址需要改成你项目中的地址，可以参考test/QQ/loginAgent.php写法
        //$qqOAuth->loginAgentUrl = 'http://localhost/test/QQ/loginAgent.php';

        //$url = $qqOAuth->getAuthUrl();
        //$_SESSION['YURUN_QQ_STATE'] = $qqOAuth->state;
        //header('location:' . $url);
    }

}
