<?php


namespace app\middleware;

use app\util\ApiLogTool;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class ApiLog implements MiddlewareInterface {

    /**
     * @param Request $request
     * @param callable $next
     * @return Response
     */
    public function process(Request $request, callable $next): Response{
        $requestInfo = $request->all();
        $response = $next($request);
        unset($requestInfo['API_CONF_DETAIL']);
        unset($requestInfo['APP_CONF_DETAIL']);

        ApiLogTool::setApiInfo((array)$request->API_CONF_DETAIL);
        ApiLogTool::setAppInfo((array)$request->APP_CONF_DETAIL);
        ApiLogTool::setRequest($requestInfo);
        ApiLogTool::setResponse($requestInfo);
        ApiLogTool::setHeader((array)$request->header());
        ApiLogTool::save();

        return $response;
    }
}
