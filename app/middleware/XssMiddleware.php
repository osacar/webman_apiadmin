<?php
namespace app\middleware;

use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class XssMiddleware implements MiddlewareInterface
{
    /**
     * 防止xss估计
     * @param Request $request
     * @param callable $handler
     * @return Response
     */

    public function process(Request $request, callable $handler): Response
    {
        if (!in_array(strtolower($request->method()), ['put', 'post'])) {
            return $handler($request);
        }
        $input = $request->all();
        array_walk_recursive($input, function(&$input) {
            $input = strip_tags($input);
        });
        $request->only($input);
        return $handler($request);
    }
}
