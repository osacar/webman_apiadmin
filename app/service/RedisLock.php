<?php

namespace app\service;

use support\Redis;

class RedisLock
{
    /**
     * redis锁
     * @param $key
     * @param int $expire
     * @param int $num
     * @return bool
     */
    public static function get_lock($key, $expire = 5, $num = 0){
        $is_lock = Redis::setnx($key, time()+$expire);
        if(!$is_lock) {
            //获取锁失败则重试{$num}次
            for($i = 0; $i < $num; $i++){
                $is_lock = Redis::setnx($key, time()+$expire);
                if($is_lock){
                    break;
                }
            }
        }
        // 不能获取锁
        if(!$is_lock){
            // 判断锁是否过期
            $lock_time = Redis::get($key);
            // 锁已过期，删除锁，重新获取
            if(time()>$lock_time){
                //释放锁
                Redis::del($key);
                $is_lock = Redis::setnx($key, time()+$expire);
            }
        }
        return $is_lock? true : false;
    }
}
