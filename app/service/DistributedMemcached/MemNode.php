<?php


namespace app\service\DistributedMemcached;


/**
 * Memcached 的节点类
 */
class MemNode
{
    private $ip = null;
    private $port = null;
    private $client = null;

    /**
     * construct
     * @param string $ip IP地址
     * @param int $port 端口号
     */
    public function __construct(string $ip, int $port)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->client = new \Memcache;
        if ($this->connect($ip, $port))
            throw new \Exception("Could not connect->".$ip.":".$port);
    }

    public function __call($name, $arguments)
    {
        // 装配调用 装饰器模式
        $this->client->$name(...$arguments);
    }

    public function __toString()
    {
        return $this->ip.':'.$this->port;
    }
}