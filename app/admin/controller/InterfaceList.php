<?php
declare (strict_types=1);
/**
 * 接口管理
 * @since   2021-11-17
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminApp;
use app\model\AdminFields;
use app\model\AdminList;
use app\util\ReturnCode;
use support\Response;

class InterfaceList extends Base {

    /**
     * 获取接口列表
     * @return Response
     * @throws \think\db\exception\DbException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function index(): Response {
        $limit =request()->get('size', config('apiwebman.ADMIN_LIST_DEFAULT'));
        $start =request()->get('page', 1);
        $keywords =request()->get('keywords', '');
        $type =request()->get('type', '');
        $status =request()->get('status', '');

        $obj = new AdminList();
        if (strlen($status)) {
            $obj = $obj->where('status', $status);
        }
        if ($type) {
            switch ($type) {
                case 1:
                    $obj = $obj->where('hash', $keywords);
                    break;
                case 2:
                    $obj = $obj->whereLike('info', "%{$keywords}%");
                    break;
                case 3:
                    $obj = $obj->whereLike('api_class', "%{$keywords}%");
                    break;
            }
        }
        $listObj = $obj->order('id', 'DESC')->paginate(['page' => $start, 'list_rows' => $limit])->toArray();

        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }

    /**
     * 获取接口唯一标识
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getHash(): Response {
        $res['hash'] = uniqid();

        return $this->buildSuccess($res);
    }

    /**
     * 新增接口
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function add(): Response {
        $postData =request()->post();
        if (!preg_match("/^[A-Za-z0-9_\/]+$/", $postData['api_class'])) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '真实类名只允许填写字母，数字和/');
        }

        $res = AdminList::create($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }
        //创建对应的api文件
        $api_class = $postData['api_class'];
        $api_data = explode("/",$api_class);
        $controller = $api_data[0];
        $action = $api_data[1];
        if($action =="index"){
            create_file_auto($controller, $action);
        }
        return $this->buildSuccess();
    }

    /**
     * 接口状态编辑
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function changeStatus(): Response {
        $hash =request()->get('hash');
        $status =request()->get('status');
        $res = AdminList::update([
            'status' => $status
        ], [
            'hash' => $hash
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }
        cache('ApiInfo' . $hash, null);

        return $this->buildSuccess();
    }

    /**
     * 编辑接口
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function edit(): Response {
        $postData =request()->post();
        if (!preg_match("/^[A-Za-z0-9_\/]+$/", $postData['api_class'])) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '真实类名只允许填写字母，数字和/');
        }

        $res = AdminList::update($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }
        cache('ApiInfo' . $postData['hash'], null);

        return $this->buildSuccess();
    }

    /**
     * 删除接口
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function del(): Response {
        $hash =request()->get('hash');
        if (!$hash) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }

        $hashRule = (new AdminApp())->whereLike('app_api', "%$hash%")->select();
        if ($hashRule) {
            $oldInfo = (new AdminList())->where('hash', $hash)->find();
            foreach ($hashRule as $rule) {
                $appApiArr = explode(',', $rule->app_api);
                $appApiIndex = array_search($hash, $appApiArr);
                array_splice($appApiArr, $appApiIndex, 1);
                $rule->app_api = implode(',', $appApiArr);

                $appApiShowArrOld = json_decode($rule->app_api_show, true);
                $appApiShowArr = $appApiShowArrOld[$oldInfo->group_hash];
                $appApiShowIndex = array_search($hash, $appApiShowArr);
                array_splice($appApiShowArr, $appApiShowIndex, 1);
                $appApiShowArrOld[$oldInfo->group_hash] = $appApiShowArr;
                $rule->app_api_show = json_encode($appApiShowArrOld);

                $rule->save();
            }
        }

        AdminList::destroy(['hash' => $hash]);
        AdminFields::destroy(['hash' => $hash]);

        cache('ApiInfo' . $hash, null);

        return $this->buildSuccess();
    }

    /**
     * 刷新接口路由
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function refresh(): Response {
        $apiRoutePath = base_path() . '/route/api.php';
        $methodArr = ['*', 'POST', 'GET'];
        $tplOriginStr = file_get_contents($apiRoutePath);
        $listInfo = (new AdminList())->where('status', 1)->select();
        $tplStr = <<<EOF
<?php

use Webman\Route;

Route::group('/api', function() {\r\n
EOF;
        foreach ($listInfo as $value) {
            $data_con = explode("/",$value->api_class);
            if ($value['hash_type'] === 1) {
                $tplStr .= <<<EOF
                
    Route::any('/{$value->api_class}',[\app\api\controller\\{$data_con[0]}::class,'$data_con[1]'])->middleware([\app\middleware\ApiAuth::class, \app\middleware\ApiPermission::class, \app\middleware\ApiLog::class]);\r\n       
EOF;
            } else {
                $tplStr .= <<<EOF
                
    Route::any('/{$value->hash}',[\app\api\controller\\{$data_con[0]}::class,'$data_con[1]'])->middleware([\app\middleware\ApiAuth::class, \app\middleware\ApiPermission::class, \app\middleware\ApiLog::class]);\r\n           
EOF;

            }
        }
        $tplStr .= <<<EOF
        \r\n
});
EOF;
;

        file_put_contents($apiRoutePath, $tplStr);

        return $this->buildSuccess();
    }

}
