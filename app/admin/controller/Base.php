<?php
declare (strict_types=1);
/**
 * 工程基类
 * @since   2017/02/28 创建
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminExecutionLog;
use app\model\AdminUser;
use app\model\AdminUserData;
use app\util\ReturnCode;
use app\BaseController;
use support\Redis;
use support\Response;
class Base extends BaseController {

    private $debug = [];

    /**
     * 构造方法
     */
    public function beforeAction()
    {
        $request = request();
        $controller = $request->controller;
        $action = $request->action;
        $user_id = session('admin_user_id',1);
        $app = $request->app;
        Redis::setEx($app."beforeAction".$controller."\\".$action.$user_id,10,microtime(true));
    }

    /**
     * 成功的返回
     * @param array $data
     * @param string $msg
     * @param int $code
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function buildSuccess(array $data = [], string $msg = '操作成功', int $code = ReturnCode::SUCCESS): Response {
        $return = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];
        $return['debug'] = uniqid();
        return json($return);
    }

    /**
     * 更新用户信息
     * @param array $data
     * @param bool $isDetail
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function updateUserInfo(array $data, bool $isDetail = false): void {
        $apiAuth = request()->header('Api-Auth');
        if ($isDetail) {
            AdminUserData::update($data, ['uid' => $this->userInfo['id']]);
            $this->userInfo['userData'] = (new AdminUserData())->where('uid', $this->userInfo['id'])->find();
        } else {
            AdminUser::update($data, ['id' => $this->userInfo['id']]);
            $detail = $this->userInfo['userData'];
            $this->userInfo = (new AdminUser())->where('id', $this->userInfo['id'])->find();
            $this->userInfo['userData'] = $detail;
        }

        cache('Login' . $apiAuth, json_encode($this->userInfo), config('apiwebman.ONLINE_TIME'));
    }

    /**
     * 错误的返回
     * @param int $code
     * @param string $msg
     * @param array $data
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function buildFailed(int $code, string $msg = '操作失败', array $data = []): Response {
        $return = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];
        $return['debug'] = uniqid();
        return json($return);
    }

    /**
     * debug参数收集
     * @param $data
     * @author bubaishaolong <584887013@qq.com>
     */
    protected function debug($data): void {
        if ($data) {
            $this->debug[] = $data;
        }
    }

    /**
     * 构造方法
     */
    public function afterAction()
    {
        $request = request();
        $user_id = session('admin_user_id',1);
        $controller = $request->controller;
        $action = $request->action;
        $app = $request->app;
        //开始执行时间
        $beforeAction = Redis::get($app."beforeAction".$controller."\\".$action.$user_id);
        //结束执行时间
        $afterAction = microtime(true);
        //方法执行的时间(ms)
        //$execution_time = bcmul(bcsub($afterAction,$beforeAction,5),1000,3);
        $execution_time = round(($afterAction-$beforeAction)*1000,5);
        $log =[
            'user_id'=>$user_id,
            'controller'=>$controller,
            'action'=>$action,
            'app'=>$app,
            'execution_time'=>$execution_time,
            'create_time'=>date('Y-m-d H:i:s'),
        ];
        AdminExecutionLog::insert($log);
    }
}
