<?php
declare (strict_types=1);
/**
 *
 * @since   2021-11-17
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminApp;
use app\model\AdminAppGroup;
use app\util\ReturnCode;
use support\Response;


class AppGroup extends Base {

    /**
     * 获取应用组列表
     * @return \think\Response
     * @throws \think\db\exception\DbException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function index(): Response {
        $limit =request()->get('size', config('apiwebman.ADMIN_LIST_DEFAULT'));
        $start =request()->get('page', 1);
        $keywords =request()->get('keywords', '');
        $type =request()->get('type', '');
        $status =request()->get('status', '');

        $obj = new AdminAppGroup();
        if (strlen($status)) {
            $obj = $obj->where('status', $status);
        }
        if ($type) {
            switch ($type) {
                case 1:
                    if (strlen($keywords)) {
                        $obj = $obj->where('hash', $keywords);
                    }
                    break;
                case 2:
                    $obj = $obj->whereLike('name', "%{$keywords}%");
                    break;
            }
        }
        $listObj = $obj->paginate(['page' => $start, 'list_rows' => $limit])->toArray();

        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }

    /**
     * 获取全部有效的应用组
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getAll(): Response {
        $listInfo = (new AdminAppGroup())->where(['status' => 1])->select();

        return $this->buildSuccess([
            'list' => $listInfo
        ]);
    }

    /**
     * 应用组状态编辑
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function changeStatus(): Response {
        $id =request()->get('id');
        $status =request()->get('status');
        $res = AdminAppGroup::update([
            'id'     => $id,
            'status' => $status
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 添加应用组
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function add(): Response {
        $postData =request()->post();
        $res = AdminAppGroup::create($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 应用组编辑
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function edit(): Response {
        $postData =request()->post();
        $res = AdminAppGroup::update($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 应用组删除
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function del(): Response {
        $hash =request()->get('hash');
        if (!$hash) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }

        $has = (new AdminApp())->where(['app_group' => $hash])->count();
        if ($has) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '当前分组存在' . $has . '个应用，禁止删除');
        }

        AdminAppGroup::destroy(['hash' => $hash]);

        return $this->buildSuccess();
    }
}
